<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('chat');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/id', 'HomeController@me');

Route::get('/id/{id}/wall', 'WallController@getCurrentWall');
Route::delete('/id/{id}', 'WallController@destroy');
Route::post('/id', 'WallController@store');
Route::get('/id/{id}', 'WallController@index');
Route::put('/id/{id}', 'WallController@update');

Route::post('/action/hide/{id}', 'ActionsController@hide');
Route::post('/action/like/{id}', 'ActionsController@like');

Route::get('/comment', 'CommentController@index');
Route::post('/comment', 'CommentController@store');
Route::delete('/comment/{id}', 'CommentController@destroy');
Route::put('/comment/{id}', 'CommentController@update');
