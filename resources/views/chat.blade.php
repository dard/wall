@extends('app')
@section('chat')
  <div class="container col-md-3" id="chat">
  </div>
  <div class="container col-md-7" id="wall">
  </div>
  <div class="btn-group manage" id="filter" role="group" aria-label="...">
    <button type="button" class="btn btn-xs btn-default button-by-like">by like</button>
    <button type="button" class="btn btn-xs btn-default button-by-date">by date</button>
  </div>
  @if (!Auth::guest())
  <script>
    sessionStorage.setItem('uid', {{ Auth::user()->id }} );
  </script>
  @endif
@endsection
