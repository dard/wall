$(function() {

  $(document).on('submit', 'form', function(){
    return false;
  });

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    error : function(jqXHR, textStatus, errorThrown) {

    }
  });
  var params = {};
  var init = function(){
    getOwnerId();
    wallBulder();
    socketCall();
  };
  var getOwnerId = function(){
    params.uid =  sessionStorage.getItem('uid');
  }

  var socketCall =function(){
    var socket = io('http://localhost:8890');
    socket.on('addMessage', function (data) {
      var html = buildHTML.addWallMsg(JSON.parse(data));
      $("#wall").append(html);
    });

    socket.on('deleteMessage', function (obj) {
      var data = JSON.parse(obj);
      if(data.status === true){
        $('.msg.container[data-mainid="'+data.parent_id+'"]').remove();
      }
    });

    socket.on('updateMessage', function (obj) {
      var data = JSON.parse(obj);
      if(data.status === true){
        $('.main-msg[data-mainid="'+data.id+'"] span.text-msg').first().text(data.message);
      }
    });

    socket.on('addComment', function (data) {
      var obj = JSON.parse(data);
      var html = buildHTML.addCommentMsg(obj);
      if(obj.child_id === null){
        $('.msg.container[data-mainid="'+obj.parent_id+'"]').append(html);
      } else {
        $('.msg.container[data-cid="'+obj.child_id+'"]').append(html);
      }
      $('.wall-popup').remove();
    });

    socket.on('deleteComment', function (obj) {
      var data = JSON.parse(obj);
      if(data.status === true){
        $('.child-msg.container[data-cid="'+data.id+'"]').remove();
      }
    });

    socket.on('updateComment', function (obj) {
      var data = JSON.parse(obj);
      if(data.status === true){
        $('.child-msg[data-cid="'+data.id+'"] span.text-msg').first().text(data.message);
      }
    });

  };

  var buildHTML = new function(){
    this.dom = '';
    this.getDom = function(){
      return this.dom;
    }
    this.setDom = function(that){
      this.dom = that;
    }
    this.addWallMsg = function(obj){
      var liked = obj.data.liked_id ? 'liked' : '';
      var likesCount = obj.data.likes_count ? obj.data.likes_count : '0';
      if(!!obj.data.ignored_by){
        return ` <div class="main-msg msg container col-md-12" data-uid=`+obj.data.user_id+` data-mainid=`+obj.data.id+`>
          <a href='/id/`+obj.data.user_id+`'><span class="user-name col-md-12">`+obj.data.user_name+`</span></a>
          <div class="text-container col-md-12">`+this.buildHiddenMessage()+``;
      }
      var res = ` <div class="main-msg msg container col-md-12" data-uid=`+obj.data.user_id+` data-mainid=`+obj.data.id+`>
        <a href='/id/`+obj.data.user_id+`'><span class="user-name col-md-12">`+obj.data.user_name+`</span></a>
        <div class="text-container col-md-12"><span class="text-msg">`+obj.data.description+`</span>`;

      if(obj.data.user_id == params.uid){
        res += `<div class="btn-group manage" role="group" aria-label="...">
        <button type="button" class="btn btn-xs btn-default button-edit-msg">edit</button>
        <button type="button" class="btn btn-xs btn-default button-delete-msg">delete</button>
        <button type="button" class="btn btn-xs btn-default button-comment-msg">comment</button></div>
        <div class="like `+liked+`"><i class="fa fa-heart" aria-hidden="true"><span class="count">`+likesCount+`</span></div></i>`
      } else {
        res += `<div class="btn-group manage" role="group" aria-label="...">
        <button type="button" class="btn btn-xs btn-default button-hide-msg">hide</button>
        <button type="button" class="btn btn-xs btn-default button-comment-msg">comment</button></div>
        <div class="like `+liked+`"><i class="fa fa-heart" aria-hidden="true"><span class="count">`+likesCount+`</span></div></i>`
      }
      res += `</div></div>`;
      return res;
    }

    this.addCommentMsg = function(obj){
      var liked = obj.liked_id ? 'liked' : '';
      var likesCount = obj.likes_count ? obj.likes_count : '0';
      var res = ` <div class="child-msg msg container comment col-md-12" data-uid=`+obj.user_id+` data-cid=`+obj.id+`>
        <a href='/id/`+obj.user_id+`'><span class="user-name col-md-12">`+obj.user_name+`</span></a>
        <div class="text-container col-md-12">`;
      if(!!obj.ignored_by){
        res += this.buildHiddenMessage();
      } else {
        res += `<span class="text-msg">`+obj.description+`</span>`;
        if(obj.user_id == params.uid){
          res += `<div class="btn-group manage" role="group" aria-label="...">
          <button type="button" class="btn btn-xs btn-default button-edit-comment">edit</button>
          <button type="button" class="btn btn-xs btn-default button-delete-comment">delete</button>
          <button type="button" class="btn btn-xs btn-default button-comment-comment">comment</button></div>
          <div class="like `+liked+`"><i class="fa fa-heart" aria-hidden="true"><span class="count">`+likesCount+`</span></div></i>`
        } else {
          res += `<div class="btn-group manage" role="group" aria-label="...">
          <button type="button" class="btn btn-xs btn-default button-hide-comment">hide</button>
          <button type="button" class="btn btn-xs btn-default button-comment-comment">comment</button></div>
          <div class="like `+liked+`"><i class="fa fa-heart" aria-hidden="true"><span class="count">`+likesCount+`</span></div></i>`
        }
      }

      res += `</div></div>`;
      return res;
    }

    this.buildInputForm = function(){
      var html = `<div>
        <form id="wall-post">
          <div class="form-group">
            <label for="wall-text">File input</label>
            <textarea class="form-control" id="wall-text" rows="3"></textarea>
          </div><button type="submit" class="btn btn-default">Submit</button>
        </form>
      </div>`;
      return html;
    };

    this.buildCommentForm = function(){
      var html = `<div class='wall-popup col-md-12'>
        <form class="wall-comment">
          <div class="form-group">
            <div class="input-group comment-group col-lg-6">
              <input type="text" class="form-control comment">
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-default btn-xs">Submit</button>
                </span>
            </div>
          </div>
        </form>
      </div>`;
      return html;
    };

    this.buildEditForm = function(obj){
      var html = `<div class='edit-popup col-md-12'>
        <form class="edit-msg">
          <div class="form-group">
            <div class="input-group comment-group col-lg-6">
              <input type="text" class="form-control comment" data-url="`+obj.url+`" data-id="`+obj.id+`" value="`+obj.msg+`">
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-default btn-xs">Submit</button>
                </span>
            </div>
          </div>
        </form>
      </div>`;
      return html;
    };

    this.buildHiddenMessage = function(){
      return '<span class="hidden-msg">HIDEN MESSAGE</span>';
    }

    this.unhideMessage = function(){
      return `<span class="text-msg">test</span>
              <div class="btn-group manage" role="group" aria-label="...">
                <button type="button" class="btn btn-xs btn-default button-hide-comment">hide</button>
                <button type="button" class="btn btn-xs btn-default button-comment-comment">comment</button>
              </div>`;
    }
  };

  var wallBulder = function(attributes){
    var orderBy = localStorage.getItem('ordered');
    console.log(window.location.pathname);
    $.ajax({
      url: window.location.pathname + '/wall',
      method: "GET",
      dataType: "json",
      data: {ordered : orderBy}
    }).done(function(data){
      console.log(data);
      if(data.owner && !(typeof attributes != 'undefined' && attributes.rebuild)){
        $('#chat').append(buildHTML.buildInputForm());
      }

      $.each(data.data, function( index, value ){
        var html = buildHTML.addWallMsg({
          data:value, owner: data.owner
        });
        $('#wall').prepend(html);
      });

      $.each(data.comments, function( index, value ) {
        var postId = index;
        if (data.comments.hasOwnProperty(index)) {
          $.each(data.comments[index], function( index, value ) {
            if(value.child_id === null){
              var html = buildHTML.addCommentMsg(value);
              $('.main-msg[data-mainid="'+postId+'"]').append(html);
            } else {
              var html = buildHTML.addCommentMsg(value);
              $('.child-msg[data-cid="'+value.child_id+'"]').append(html);
            }
          });
        }
      });
    });
  };

  $('.button-by-date, .button-by-like').click(function(e){
    $('#wall').empty();
    var target = e.target;
    var obj = {rebuild : true};
    if($(target).hasClass('button-by-like')){
      if(localStorage.getItem('ordered') && localStorage.getItem('ordered') == 'by_like'){
        localStorage.removeItem('ordered');
      } else {
        localStorage.setItem('ordered', 'by_like');
      }
    } else if($(target).hasClass('button-by-date')){
      if(localStorage.getItem('ordered') && localStorage.getItem('ordered') == 'by_date'){
        localStorage.removeItem('ordered');
      } else {
        localStorage.setItem('ordered', 'by_date');
      }
    }
    wallBulder(obj);
  });

  $(document).on('click', '.button-comment-msg, .button-comment-comment', function(){
    $('.wall-popup').remove();
    $(this).closest('.msg').append(buildHTML.buildCommentForm());
  });

  $(document).on('click', '.button-hide-comment, .button-hide-msg', function(){
    var dom = $(this).closest('.msg.container');
    if($(dom).attr('data-cid')){
      var id = $(dom).attr('data-cid');
      var status = 'comment';
    } else {
      var id = $(dom).attr('data-mainid');
      var status = 'post';
    }
    $.ajax({
      url: '/action/hide/'+id,
      method: "POST",
      dataType: "json",
      data: {status : status}
    }).done(function(data){
      if(data.status === true){
        $(dom).find('.text-container').first().html(buildHTML.buildHiddenMessage());
        // $(dom).find('.user-name').first().css({opacity:0.2});
      }
    });
  });

  $(document).on('click', 'div.like', function(){
    var dom = $(this).closest('.msg.container');
    if($(dom).attr('data-cid')){
      var id = $(dom).attr('data-cid');
      var status = 'comment';
    } else {
      var id = $(dom).attr('data-mainid');
      var status = 'post';
    }

    $.ajax({
      url: '/action/like/'+id,
      method: "POST",
      dataType: "json",
      data: {status : status}
    }).done(function(data){
      if(data.status === true){
        $(dom).find('.like').first().addClass('liked');
        $(dom).find('.count').first().html(data.count);
      }else if(data.status === false){
        $(dom).find('.like').first().removeClass('liked');
        $(dom).find('.count').first().html(data.count);
      }
    });
  });

  $(document).on('click', '.button-edit-msg, .button-edit-comment', function(e){
    $('.edit-popup').remove();
    var obj = {};
    var dom = $(this).closest('.msg.container');
    obj.msg = $(dom).find('span.text-msg').first().text();
    if($(dom).attr('data-mainid')){
      obj.id = $(dom).attr('data-mainid')
      obj.url = 'id';
    } else if($(dom).attr('data-cid')){
      obj.id = $(dom).attr('data-cid');
      obj.url = 'comment';
    }
    var html = buildHTML.buildEditForm(obj);
    $(dom).append(html);
  });

  $(document).mouseup(function (e){
    var div = $(".edit-popup");
    if (!div.is(e.target)
        && div.has(e.target).length === 0) {
      div.remove();
    }
  });

  // $(document).on('keyup', 'input.comment', function(){
  //   var value = $(this).val();
  //   $(this).closest('.msg.container').find('span.binded').text(value);
  // });

  $(document).on('click', '.button-delete-msg', function(){
    var userId = $(this).closest('.msg.container').attr('data-uid');
    var contentId = $(this).closest('.msg.container').attr('data-mainid');
    var that = this;
    $.ajax({
      url: '/id/'+contentId,
      method: "DELETE",
      dataType: "json",
      data: {userId : userId, contentId : contentId},
    }).done(function(data){
    });
  });

  $(document).on('click', '.button-delete-comment', function(){
    var userId = $(this).closest('.child-msg').attr('data-uid');
    var commentId = $(this).closest('.child-msg').attr('data-cid');
    var mainId = $(this).closest('.main-msg').attr('data-mainid');
    var that = this;
    $.ajax({
      url: '/comment/'+commentId,
      method: "DELETE",
      dataType: "json",
      data: {user_id : userId, comment_id : commentId, parent_id : mainId},
    }).done(function(data){
    });
  });


  $(document).on('submit', 'form.wall-comment', function(){
    var msg = $('.wall-comment .comment').val();
    var parentContainer = $(this).closest('.main-msg.container');
    var childContainer = $(this).closest('.child-msg.container');
    var contentId = $(parentContainer).attr('data-mainid');
    var childId = $(childContainer).length ? $(childContainer).attr('data-cid') : null  ;
    var ownerId = $(parentContainer).attr('data-uid');
    var that = this;
    buildHTML.setDom(that);
    $.ajax({
      url: '/comment',
      method: "POST",
      dataType: "json",
      data: {parent_id : contentId, child_id : childId,user_id : ownerId, message : msg}
    }).done(function(data){
    });
  });

  $(document).on('submit', 'form#wall-post', function(){
    var msg = $('#wall-post #wall-text').val();
    buildHTML.setDom(this);
    $.ajax({
      url: '/id',
      method: "POST",
      dataType: "json",
      data: {message : msg}
    }).done(function(data){
      $("form#wall-post").trigger("reset");
    });
  });

  $(document).on('submit', 'form.edit-msg', function(){
    var dom = $('input.comment');
    var postId = $(dom).attr('data-id');
    var url = $(dom).attr('data-url');
    var text = $(dom).val();
    $.ajax({
      url: '/'+url+'/'+postId,
      method: "PUT",
      dataType: "json",
      data: {message : text},
    }).done(function(data){
      $('.edit-popup').remove();
    });
  });

  init();

});
