var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var redis = require('redis');

http.listen(8890);
io.on('connection', function (socket) {

  console.log("new client connected");
  var redisClient = redis.createClient();
  redisClient.subscribe('addMessage');
  redisClient.subscribe('deleteMessage');
  redisClient.subscribe('updateMessage');

  redisClient.subscribe('addComment');
  redisClient.subscribe('deleteComment');
  redisClient.subscribe('updateComment');
  
  redisClient.on("message", function(channel, message) {
    console.log("mew message in queue "+ channel + "channel");
    socket.emit(channel, message);
  });


  socket.on('disconnect', function() {
    redisClient.quit();
  });

});
