<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Actions;
use TRedis;

class ActionsController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Hide a comment or post.
   *
   * @return \Illuminate\Http\Response
   */
  public function hide(Request $request, $id)
  {
    $this->validate($request,[
      'status' => 'required|string|max:50',
    ]);
    $res = Actions::setHide($request, $id);
    return response()->json(['status'=>$res]);
  }

  /**
   * Like a comment or post.
   *
   * @return \Illuminate\Http\Response
   */
  public function like(Request $request, $id)
  {
    $this->validate($request,[
      'status' => 'required|string|max:50',
    ]);
    $res = Actions::setLike($request, $id);
    return response()->json($res);
  }
}
