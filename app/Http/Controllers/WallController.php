<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Wall;
use TRedis;

class WallController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        TRedis::connection();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('chat');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Wall $wall)
    {
      $this->validate($request,[
        'message' => 'required|max:300',
      ]);
      $res = $wall->addPost($request);
      TRedis::publish('addMessage', json_encode(['data'=> $res]));
      return response()->json(['owner'=>true, 'data'=> $res]);
    }

    /**
     * Get current users wall's records.
     *
     * @param  int  $id
     * @param Wall is instance of Wall class
     * @return \Illuminate\Http\Response
     */
    public function getCurrentWall($id = null, Wall $wall, Request $request)
    {
      $this->validate($request,[
        'ordered' => 'string|max:300|nullable',
      ]);
      $res = $wall->getPosts($request, $id);
      return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // var_dump($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  App\Wall $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wall $wall, $id)
    {
      $this->validate($request,[
        'message' => 'required|max:300',
      ]);
      $res = $wall->updatePost($request, $id);
      TRedis::publish('updateMessage', json_encode(['status'=>$res, 'message'=>$request->message, 'id'=>$id]));
      return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  App\Wall $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Wall $wall, $id)
    {
      $res = $wall->deletePost($request, $id);
      TRedis::publish('deleteMessage', json_encode($res));
      return response()->json($res);
    }
}
