<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Comment;
use TRedis;

class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        TRedis::connection();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Comment $comment)
    {
      $this->validate($request,[
        'message' => 'required|max:300',
        'user_id' => 'required|numeric',
        'parent_id' => 'required|numeric',
        'child_id' => ['required', 'nullable', 'numeric']
      ]);
      $res = $comment->addComment($request);
      TRedis::publish('addComment', json_encode($res));
      return response()->json($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'message' => 'required|max:300',
      ]);
      $res = Comment::updateComment($request, $id);
      TRedis::publish('updateComment', json_encode(['status'=>$res, 'message'=>$request->message, 'id'=>$id]));
      return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      $res = Comment::deleteComment($id);
      TRedis::publish('deleteComment', json_encode(['status'=>$res,'id'=>$id]));
      return response()->json($res);
    }
}
