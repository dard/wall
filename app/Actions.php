<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Actions extends Model
{

  public static function setLike(Request $request, $id)
  {
    $uid = Auth::id();
    $res = ['data'=>$id, 'type'=>$request->status];
    $ifExist = DB::table('likes')->where([['post_id','=', $id],
                                          ['user_id', '=', $uid],
                                          ['status', '=', $request->status]
                                         ])->first();
    if($ifExist){
      $res['status'] = !(boolean) DB::table('likes')->where(['post_id' => $id,
                                                    'user_id' => $uid,
                                                    'status' => $request->status
                                                  ])->delete();
    } else {
      $res['status'] = (boolean) DB::table('likes')->insert(['post_id' => $id,
                                                   'user_id' => $uid,
                                                   'status' => $request->status,
                                                   'created_at' => Carbon::now(),
                                                   'updated_at' => Carbon::now()
                                                 ]);
    }
      $res['count'] = self::getSinglePostLikes($id, $request->status);
      return $res;
  }

  /**
   * Update single comment
   * @param int $id: record identifier
   * @return boolean result of hide operation
   */
  public static function setHide($request, $id)
  {
    $uid = Auth::id();
    $ifExist = DB::table('ignored')->where([['post_id','=', $id],
                                            ['user_id', '=', $uid],
                                            ['status', '=', $request->status]
                                           ])->first();
    if($ifExist){
      return !(boolean) DB::table('ignored')->where(['post_id' => $id,
                                                      'user_id' => $uid,
                                                      'status' => $request->status
                                                    ])->delete();
    }
    return (boolean) DB::table('ignored')->insert(['post_id' => $id,
                                                   'user_id' => $uid,
                                                   'status' => $request->status
                                                 ]);
  }

  public static function getPostLikes(array $where, $type)
  {
    return DB::table('likes')->select(DB::raw('count(likes.id) as like_count'), 'likes.post_id')
                                ->whereIn('post_id', $where)
                                ->where('status', '=', $type)
                                ->groupBy('likes.post_id')
                                ->get();
  }

  public static function filterForOrdered(Request $request)
  {
    $order = $request->ordered;
    $orderBy = ['created_at', 'desc'];
    switch ($order) {
      case 'by_date':
        $orderBy = ['created_at', 'asc'];
        break;
      case 'by_like':
        $orderBy = ['created_at', 'asc'];
        break;
    }
    return $orderBy;
  }

  public static function getSinglePostLikes($id, $type)
  {
    return DB::table('likes')->select('id')
                                ->where([['status', '=', $type], ['post_id', '=', $id]])
                                ->count();
  }
}
