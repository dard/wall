<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\Actions;
use Auth;
use Carbon\Carbon;

class Comment extends Model
{
  protected $table = 'comments';
  protected $fillable = ['user_id', 'parent_id', 'child_id', 'description'];

  /**
   * add comment row
   * @param Illuminate\Http\Request $request
  * @return array: contains new comment data
   */
  public function addComment(Request $request)
  {
    $createdId = DB::table('comments')->insertGetId(
      [ 'user_id' => Auth::id(),
        'parent_id' => $request->parent_id,
        'child_id' => $request->child_id,
        'description' => $request->message,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);
    $data = Comment::select('id', 'user_id', 'parent_id', 'child_id', 'description', 'created_at')->find($createdId);
    $data['user_name'] = Auth::user()->name;
    return $data;
  }

  /**
   * return comments array.
   *
   * @param int $idWall: means whose 'wall' must be loaded
   * @param Illuminate\Http\Request $request
   * @return array
   */
  public static function getComments($idWall)
  {
    $res = DB::table('wall')->select('wall.id')->where('user_id', '=', $idWall)->get();
    $where = [];
    foreach ($res as $key => $value) {
      $where[] = $value->id;
    }
    $likesCount = Actions::getPostLikes($where, 'comment');

    $res = self::getCommentsByPostIds($where);

    $res = self::combineLikePost($res, $likesCount);
    return $res;
  }

  /**
   * Combine 2 arrays in one by post id
   * @param array $res: contains comments query
   * @param array $likesCount: contains likes query
   * @return array
   */
  public static function combineLikePost($res, $likesCount)
  {
    foreach ($res as $key => $value){
      $res[$key]->likes_count = null;
      foreach ($likesCount as $item) {
        if($value->id == $item->post_id){
          $res[$key]->likes_count = $item->like_count;
        }
      }
    }
    return $res;
  }

  /**
   * Get all comments from current wall according to posts id's
   * @param array $where: contains id's of all posts
   * @return database query object
   */
  private static function getCommentsByPostIds($where)
  {
    return DB::table('comments')
            ->select('users.name as user_name', 'ignored.post_id as ignored_post_id',
                     'ignored.user_id as ignored_by', 'comments.user_id', 'comments.id',
                     'comments.parent_id', 'comments.description',
                     'comments.created_at', 'comments.child_id', 'likes.post_id as liked_id')
            ->join('users', 'comments.user_id', '=', 'users.id')
            ->leftJoin('ignored', function ($join) {
              $join->on('comments.id', '=', 'ignored.post_id')->where([['ignored.user_id', '=', Auth::id()], ['ignored.status', '=', 'comment']]);
            })
            ->leftJoin('likes', function ($join) {
              $join->on('comments.id', '=', 'likes.post_id')->where([['likes.user_id', '=', Auth::id()], ['likes.status', '=', 'comment']]);
            })
            ->whereIn('parent_id', $where)
            ->orderBy('created_at')->get();
  }



  /**
   * Update single comment
   * @param int $id: comment id
   * @param Illuminate\Http\Request $request
   * @return boolean result of update operation
   */
  public static function updateComment(Request $request, $id)
  {
    return (boolean) Comment::where([['id', $id], ['user_id', Auth::id()]])
                          ->update(['description' => $request->message]);
  }

  /**
   * delete comment row
   * @param int $id: comment identifier
   * @return boolean: delete operation result
   */
  public static function deleteComment($id)
  {
    return (boolean) Comment::destroy($id);
  }

}
