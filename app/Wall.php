<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\Actions;
use Auth;
use App\Comment;
use Carbon\Carbon;

class Wall extends Model
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'user_id', 'description'
  ];
  protected $table = 'wall';

  /**
   * Add main wall massage
   * @param Illuminate\Http\Request $request
   * @return array
   */
  public function addPost(Request $request)
  {
    $createdId = DB::table('wall')->insertGetId(
      [ 'user_id' => Auth::id(),
        'description' => $request->message,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
      ]);
    $res = Wall::select('id', 'user_id', 'description', 'created_at')->find($createdId);
    $res['user_name'] = Auth::user()->name;
    return $res;
  }

  /**
   * get main wall massage and all relatives comments
   * @param $id post identifier
   * @return object of existing rows
   */
  public function getPosts($request, $id)
  {
    $authId = Auth::user()->id;
    $res = [];
    $where = [];

    if($id == $authId){
      $res['owner'] = true;
      $idWall = $authId;
    } else {
      $res['owner'] = false;
      $idWall = $id;
    }

    $order = $request->ordered;
    $orderBy = ['created_at', 'desc'];
    $likeOrder = false;
    switch ($order) {
      case 'by_date':
        $orderBy = ['created_at', 'asc'];
        break;
      case 'by_like':
        $likeOrder = true;
        break;
    }

    //create action for order operation
    // $orderBy = Actions::filterForOrdered($request);
    //get all main posts from current wall
    $res['data'] = self::getPostsByIdWall($idWall, $orderBy);
    //get all comments accorting to current wall id
    $comments = Comment::getComments($idWall);
    // distribute comments, depending on the post id
    foreach ($res['data'] as $item) {
      foreach ($comments as $key => $value) {
        if($item->id == $value->parent_id){
          $res['comments'][$item->id][] = $value;
        }
      }
      $where[] = $item->id; //all main posts id's
    }

    $likesCount = Actions::getPostLikes($where, 'post');
    $res['data'] = Comment::combineLikePost($res['data'], $likesCount);
    $res['data'] = $this->filterPostByLikeCnt($res['data'], $likeOrder);
    return (object) $res;
  }

  /**
   * sort array by user likes if status true
   * @param $arr array of wall records
   * @return array
   */
  public function filterPostByLikeCnt($arr, $status = false)
  {
    $toArr = $arr->toArray();
    if($status === true){
      usort($toArr, function($a, $b){
        return strcmp($a->likes_count, $b->likes_count);
      });
    }
    return $toArr;
  }

  /**
   * get all posts according to idWall
   * @param $idWall: current wall id
   * @param $orderBy: array for orderBy query
   * @return object: query
   */
  private static function getPostsByIdWall($idWall, $orderBy)
  {
    return DB::table('wall')->select('wall.id', 'wall.user_id', 'wall.description',
                                              'wall.created_at', 'users.name as user_name',
                                              'likes.post_id as liked_id', 'ignored.post_id as ignored_post_id', 'ignored.user_id as ignored_by')
                      ->join('users', 'wall.user_id', '=', 'users.id')
                      ->leftJoin('ignored', function ($join) {
                        $join->on('wall.id', '=', 'ignored.post_id')
                              ->where([['ignored.user_id', '=', Auth::id()], ['ignored.status', '=', 'post']]);
                      })
                      ->leftJoin('likes', function ($join) {
                        $join->on('wall.id', '=', 'likes.post_id')
                              ->where([['likes.user_id', '=', Auth::id()], ['likes.status', '=', 'post']]);
                      })
                      ->where('wall.user_id', '=', $idWall)
                      ->orderBy(...$orderBy)
                      ->get();
  }

  /**
   * update main post row
   * @param $id post identifier
   * @param Illuminate\Http\Request $request
   * @return boolean: update operation result
   */
  public function updatePost(Request $request, $id)
  {
    return (boolean) Wall::where([['id', $id], ['user_id', Auth::id()]])
                          ->update(['description' => $request->message]);
  }

  /**
   * delete main post row
   * @param $contentId: post identifier
   * @param Illuminate\Http\Request $request
   * @return boolean: delete operation result
   */
  public function deletePost(Request $request, $contentId)
  {
    $uid = $request['userId'];
    if($uid == Auth::user()->id){
      $res['status'] = (boolean) Wall::destroy($contentId);
      $res['comments'] = (boolean) Comment::where('parent_id', '=', $contentId)->delete();
      $res['parent_id'] = $contentId;
      return $res;
    }
    return false;
  }

}
