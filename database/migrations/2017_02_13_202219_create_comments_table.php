<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('comments', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('child_id')->unsigned()->nullable();
        $table->integer('parent_id');
        $table->integer('user_id');
        $table->text('description');
        $table->timestamps();
        $table->index(['child_id', 'parent_id', 'user_id']);
      });

      Schema::table('comments', function (Blueprint $table) {
        $table->foreign('child_id')->references('id')->on('comments')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('comments');
    }
}
