<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use Auth;
use TRedis;
use Faker\Factory;
class WallTest extends TestCase
{
    use DatabaseTransactions;
    public function testGetCurrentWall()
    {
      $user = factory(User::class)->make();
      $response = $this->actingAs($user)->json('get', '/wall/id/'.$user->id);
      $response->assertStatus(200)->assertJsonFragment([
                'owner' => true,
                'data'=>[],
            ]);
    }

    public function testStore()
    {
      $faker = Factory::create();
      $user = factory(User::class)->make();
      $response = $this->actingAs($user)->json('POST', '/id', ['message' => $faker->text]);
      $createdContent = json_decode($response->content());
      $this->assertDatabaseHas('wall', [
          'id' => $createdContent->data->id
      ]);
      $response->assertStatus(200)->assertJsonStructure(['owner','data'=>['id', 'created_at', 'user_id', 'description', 'user_name']]);
    }

    public function testDestroy(){
      $faker = Factory::create();
      $user = factory(User::class)->make();
      $create = $this->actingAs($user)->json('POST', '/id', ['message' => $faker->text]);
      $createdContent = json_decode($create->content());
      $this->assertDatabaseHas('wall', [
          'id' => $createdContent->data->id
      ]);
      $response = $this->actingAs($user)->json('DELETE', '/id/'.$createdContent->data->id, ['userId'=>$user->id]);
      $response->assertStatus(200)->assertJsonFragment(['parent_id']);
    }
  }
